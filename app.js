
//Chargement des modules requis 
const express = require("express");
const path = require("path");
const sqlite3 = require("sqlite3").verbose();
const bodyParser = require('body-parser');
const { log } = require("console");

//Création du serveur express
const app = express();

// Configuration du serveur
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));
app.use(express.static(path.join(__dirname, "public")));
app.use(bodyParser.urlencoded({ extended: true }));

// connection à la base de données
const dbname = path.join(__dirname, "data", "apptest.db");
let db = new sqlite3.Database(dbname, err => {
  if (err) {
    return console.error(err.message);
  }
  console.log("Connexion réussie à la base de données 'apptest.db'");
});

/*Définition des routes principales*/
// GET/index
app.get("/", function (req, res) {
  res.render("index");

});

// GET/form
app.get('/form', function (req, res) {
  res.render("_form");
});
 
//calcule de l'age
function calculAge(dtn) {
  debugger
  let today = new Date();
  let birthDate = new Date(dtn);
  let age = today.getFullYear() - birthDate.getFullYear();
  let m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age = age - 1;
  }
  return age;
}
//GET/table
app.get("/table", (req, res) => {

  const sql = "SELECT * FROM Students";

  db.all(sql, [], (err, rows) => {

    if (err) {

      return console.error(err.message);

    }

    rows = rows.map(row => {

      row.age = calculAge(row.bDate);

      return row;

    });

    res.render('table', { Students: rows, });

  });

});

//POST/Insert

app.post("/addStudent", (req, res) => {
  const sql = "INSERT INTO Students(lName, fName, bDate, iCard) VALUES (?,?,?,?)";
  const params = [req.body.lastName, req.body.firstName, req.body.birthDate, req.body.idCard];
  console.log(params);
  db.run(sql, params, function (err) {
    if (err) {
      return console.error(err.message);
    }

    res.redirect('table');
  });
})

//GET/Update
app.get("/updateForm/:id", (req, res) => {
  const sql = "SELECT * FROM Students WHERE idStudents= ?";
  const params = [req.params.id];

  db.get(sql, params, (err, student) => {
    if (err) {
      return console.error(err.message);
    }

    res.render('update', { studentUpdate: student });
  });
});

//POST/Update
app.post("/updateStudent/:id", (req, res) => {
  const sql = "UPDATE Students SET idStudents = ?,lName = ?, fName = ?, bDate = ?, iCard = ? WHERE idStudents = ?";
  const params = [req.body.id, req.body.lastName, req.body.firstName, req.body.birthDate, req.body.idCard, req.body.id];

  db.run(sql, params, (err) => {
    if (err) {

      return console.error(err.message);
    }

    res.redirect('/table');
  });
});

//Get/Delete
app.get("/deleteStudent/:id", (req, res) => {
  const sql = "DELETE from Students WHERE idStudents = ?";
  const params = [req.params.id];

  db.run(sql, params, (err) => {
    if (err) {
      return console.error(err.message);
    }
  });

  res.redirect('/table');
});

// Démarrage du serveur
app.listen(3000, () => {
  console.log("Serveur démarré (http://localhost:3000/) !");
});

